defmodule Relay.MixProject do
  use Mix.Project
  def project do
    [
      app: :htrv2,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      # compilers: [:disk_state] ++ Mix.compilers,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Relay.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:poison, "~> 3.1"}, {:httpoison, "~> 1.0"}, {:html_entities, "~> 0.4.0"}
    ]
  end
end

defmodule Mix.Tasks.Compile.DiskState do
  def run(_args) do
    {result, _error_code} = System.cmd("make", [], stderr_to_stdout: true)
    IO.binwrite result
  end
end
