defmodule Relay.Posts do
  use HTTPoison.Base
  @expected_fields ~w(
    filename time no tim resto fsize ext closed name
  )
  def process_url(board) do
    "https://a.4cdn.org/#{board}/1.json"
  end
  def process_request_options(opt) do
    Keyword.put_new(opt, :pool, :chan_pool)
  end
  def process_response_body(body) do
    json = Poison.decode! body
    {flatten_images(json), diff_times(json)}
  end
  def flatten_images(xss) do
    Enum.sort_by((for xs <- xss["threads"], x <- xs["posts"], x["fsize"],
      do: x |> Map.take(@expected_fields)), fn x -> x["no"] end)
  end
  def diff_times(xss) do
    [head | tail] = Enum.sort(for xs <- xss["threads"], !Map.has_key?(List.first(xs["posts"]), "closed"),
      do: List.last(xs["posts"])["time"] * 1000); Integer.floor_div Enum.sum(for {a, b}
        <- Enum.zip([head | tail], tail), do: b - a), 5
  end
end
