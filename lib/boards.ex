defmodule Relay.Boards do
  def start_link(name \\ nil) do
    Relay.Posts.start
    Relay.global_init(fn -> cycle_boards(
      Relay.load_state(), system_time(), 285) end, name)
  end
  @delay 825
  def cycle_boards(state, start_time, elapsed) do
    new_state = for {board, [etc | fuck]} <- state,
      into: %{}, do: {board, [etc - @delay | fuck]}
    f_state = Enum.filter(Map.to_list(state), fn
      {b, _} -> Map.has_key?(Relay.board_list, b) end)
    {board, consts} = Enum.min_by(f_state,
      fn {_, [etc | _]} -> etc end); {new_elapsed, new_consts}
      = handle_board(start_time, board, elapsed, consts)
    Process.sleep(@delay - new_elapsed); end_time = system_time()
    Relay.save_state(new_state); #IO.puts "Current State: "; IO.inspect new_state;
    Map.put(new_state, board, new_consts) |> cycle_boards(end_time, new_elapsed)
  end
  def handle_board(start_time, board, elapsed, [_, ema, no]) do
    {images, offset} = Relay.Posts.get!(board).body
    newest = images |> Enum.filter(fn v -> v["no"] > no end)
    for entry <- newest do
      send Relay.Channels, {board,
        Map.put(entry, "board", board)}
    end
    new_elapsed = average(12, elapsed, system_time() - start_time)
    #IO.puts "board: /#{board}/ took #{new_elapsed}, offset: #{offset}"
    {min(new_elapsed, @delay), [ema, average(5, ema, offset), if Enum.empty? newest
      do no else List.last(newest)["no"] end]}
  end
  def average(weight, past, new) do
    :erlang.round((1 - weight / 100) * past + new * (weight / 100))
  end
  defp system_time do
    {mega, seconds, ms} = :os.timestamp()
    (mega*1000000 + seconds)*1000 + :erlang.round(ms/1000)
  end
end
