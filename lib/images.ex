defmodule Relay.Images do
  use HTTPoison.Base
  def process_request_options(opt) do
    Keyword.put_new(opt, :pool, :image_pool) |>
    Keyword.put_new(:recv_timeout, 150000)
  end
  def load_file(post) do
    cdn_link = "https://i.4cdn.org/#{post["board"]}/#{post["tim"]}#{post["ext"]}"
    img_bin = case Relay.Images.get(cdn_link) do
      {:ok, %{status_code: 404}} -> exit {404, cdn_link}
      {:error, reason} -> IO.inspect [reason, cdn_link]
        send Relay.Channels, post; exit :retry
      {:ok, image} -> image.body
    end
    {img_bin, to_string(if String.first post["filename"] do URI.encode_www_form(
      HtmlEntities.decode post["filename"]) else post["tim"] end)}
  end
end
