defmodule Relay.Channels do
  use HTTPoison.Base
  def start_link(name \\ nil) do
    Relay.Channels.start; Relay.Images.start
    Relay.global_init(&upload_image/0, name)
  end
  def process_url(board) do
    Relay.board_list[board]
  end
  def process_request_options(opt) do
    Keyword.put_new(opt, :pool, :discord_pool) |>
    Keyword.put_new(:recv_timeout, 150000) |>
    Keyword.put_new(:stream_to, self())
  end
  def process_request_body(post) do
    thread_id = if post["resto"] > 0 do post["resto"] else post["no"] end
    thread_link = "boards.4chan.org/#{post["board"]}/thread/#{thread_id}#p#{post["no"]}"
    {f_data, f_name} = Relay.Images.load_file(post)
    {:multipart, [
      {"username", "/#{post["board"]}/ #{post["name"]} [#{post["no"]}]"},
      {"content", "**[#{thread_link}](https://anon.to?https://#{thread_link})**"
        <> " `current: #{post["counter"]}`"},
      {"avatar_url", "https://i.4cdn.org/#{post["board"]}/#{post["tim"]}s.jpg"},
      {"file", f_data, {"form-data", [{"filename", f_name <> post["ext"]}]}, []}
    ]}
  end
  def process_response_body(body) do
    Poison.decode! body
  end
  def upload_image, do: upload_image 1
  def upload_image(counter) do
    receive do
      %{code: 200} -> nil
      {board, post} ->
        new_post = Map.put(post, "counter", counter)
        if :rand.uniform() < 0.85 do
          try do post!(board, new_post) catch
            :exit, {404, link} -> IO.puts "Image Deleted: #{link}"
            :exit, :retry -> IO.puts "Queued Again: #{post["no"]}"
            otherwise -> IO.inspect otherwise
          end; Process.sleep(25)
        end; Process.sleep(15)
        upload_image(counter + 1)
      %{code: status} -> IO.puts "Recieved Code (from Discord): #{status}"
      _ -> nil
    end
    upload_image(counter)
  end
end
