defmodule Relay do
  def board_list do
    Application.fetch_env!(:htrv2, :live)
  end
  @board_consts [-100, 5000, 0]
  def global_init(start, name \\ nil) do
    {status, pid} = Task.start_link start
    if name, do: Process.register pid, name
    {status, pid}
  end
  def load_state do
    default = for {board, _} <- Relay.board_list, into:
      %{}, do: {board, @board_consts}
    for term <- (try do
      :erlang.binary_to_term(File.read! "post.state")
    rescue _ -> if File.exists?("post.state.1") do
      :erlang.binary_to_term(File.read! "post.state.1")
    else default end end), into: default, do: term
  end
  def save_state(current) do
    if File.exists?("post.state"), do:
      File.rename("post.state", "post.state.1")
    File.write "post.state", :erlang.term_to_binary current
    if File.exists?("post.state.1"), do:
      File.rm("post.state.1")
  end
end
