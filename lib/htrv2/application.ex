defmodule Relay.Application do
  use Application
  def start(_type, _args) do
    import Supervisor.Spec
    #:observer.start
    children = [
      #:hackney_pool.child_spec(:discord_pool, [timeout: 150000, max_connections: 4]),
      #:hackney_pool.child_spec(:image_pool, [timeout: 150000, max_connections: 6]),
      #:hackney_pool.child_spec(:chan_pool, [timeout: 150000, max_connections: 2]),
      worker(Relay.Channels, [Relay.Channels]), worker(Relay.Boards, [Relay.Boards]),
    ]
    opts = [strategy: :one_for_one, name: Relay.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
